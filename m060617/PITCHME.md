---

# Welcome to the DSARC meeting!

---

### Tonight we will be discussing the following topics.

+ Old Business
	+ Mini Field Day Review
+ New Business
	+ Upcoming Events
	+ Club Outreach Event
+ Training
	+ Basic Electronics

---

## Old Business

+++

## Mini Field Day Spring 2017

Mini field day in review.

Questions, comments, feedback we want it all.

---

## New Business

+++

### Next Outing

Lets organize our next outing and find other clubs and regions to participate.


---

## Upcoming Events

---

## Club Outreach Event

We still need to come up with ideas.

---

## End of Meeting

As always thank you for coming to the meeting. With out your participation we wouldn't be Doing Anything.

+++

If you have any ideas for future topics or events please let me know.

+++

Also if you have a project that you want to work on, but you don't have the tools, you can always work on it here.

Any of the tools that we have can be used in-store for free, or can be checked out.

+++

### And now on to Tom with a training on basic electronics.
