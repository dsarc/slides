---

# Welcome to the DSARC meeting!

---

### Tonight we will be discussing the following topics.

+ Old Business
	+ Mini Field Day
+ New Business
	+ Other Upcoming Events
	+ Next Training Topic
	+ Club Outreach Event

---

## Upcoming Events

---

## Mini Field Day Spring 2017

<iframe src="https://dsarc.us/mini-field-day-spring-2017/" width="900" height="500"></iframe>

+++

## Registration Form Preview

<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSedqO_G-W6GOk5A-9XuEga2R_1egY8u4CrE_Wp-CFl5g73bnA/viewform?embedded=true" width="760" height="500" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>

---

## Other Upcoming Events

<iframe src="https://dsarc.us/calendar/" width="900" height="500"></iframe>

---

## Next Training Topic

We have an opportunity to have an electronics and diagnostics class presented by Tom KI6DER.
He wants to know what level to start at.

---

## Club Outreach Event
We need to come up with Ideas.

---

## End of Meeting

As always thank you for coming to the meeting. With out your participation we wouldn't be Doing Anything.

+++

If you have any ideas for future topics or events please let me know.

+++

Also if you have a project that you want to work on, but you don't have the tools, you can always work on it here.

Any of the tools that we have can be used in-store for free, or can be checked out.

+++

### We look forward to seeing everyone this weekend.

#### Have a great night.

---

## Notes
<iframe src="https://docs.google.com/document/d/1aa6RAcNtLnDZtodnAhSMZdBviPDFPylxVNS8AKB9dEY/pub?embedded=true" width="900" height="500"></iframe>

---

## Minutes

<iframe src="https://docs.google.com/document/d/1TPKjBrB1BU-dHDMfYkirBeudUOcwszU7TR2AjfqwsiU/pub?embedded=true" width="900" height="500"></iframe>