# <center>Welcome to the DSARC meeting!</center>

---

### Tonight we will be discussing the following topics.

+ New Business
	+ Upcoming Events

+ Training
	+ Winlink Using RMS Express
	+ Winlink Using Pat

---

## <center>New Business</center>

+++

### Upcoming Events

* Hells Gate - Tech License Class - March 24th and 31st
* Washington EOC Exercise - March 31st
* Newbies Training with Hells Gate - TBD

+++

### Upcoming Planning

* DSARC Tech License Class
	* We already have new interest.
	* Maybe plan for end of April or May
* Field Day
	* Need to begin planning location
	* Need to start getting advertising put together (Hmm sound like a good job for a PIO)

---

## End of Meeting

As always thank you for coming to the meeting. With out your participation we wouldn't be Doing Anything.

+++

If you have any ideas for future topics or events please let me know.

+++

Also if you have a project that you want to work on, but you don't have the tools, you can always work on it here.

Any of the tools that we have can be used in-store for free, or can be checked out.

+++

### And now on to the training.
<center>Go ahead and get some coffee.</center>